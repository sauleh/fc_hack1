from django.shortcuts import render, get_object_or_404

from assignments.solutions import get_solutions


def assignments(request):
    context = {
        'results': get_solutions()
    }
    return render(request, 'assignments.html', context)
