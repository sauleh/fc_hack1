from django.shortcuts import render, get_object_or_404
from django.views.generic import View

from poll.models import Poll, Choice


class PollView(View):

    def get(self, request, poll_id):
        context = {
            'poll': get_object_or_404(Poll, pk=poll_id),
            'choices': Choice.objects.filter(poll_id=poll_id).all()
        }
        return render(request, 'poll.html', context)
